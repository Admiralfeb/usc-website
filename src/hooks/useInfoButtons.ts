import { docsList, guidesList, toolsList, odysseyList } from 'data/information';
import { uscLinksList } from 'data/about';

export const useInfoButtons = () => {
  return { docsList, guidesList, toolsList, odysseyList, uscLinksList };
};
