import { JoinNextSteps } from 'components/join';
import Head from 'next/head';

export const JoinNextStepsPage = () => {
  return (
    <>
      <Head>
        <title>Next Steps after Joining USC</title>
      </Head>
      <JoinNextSteps />
    </>
  );
};

export default JoinNextStepsPage;
